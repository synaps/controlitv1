function HomeReady()
{
    GetSotialButtons();
}

function ContactReady()
{

    jQuery('button[type="submit"]').bind('click', function(event)
    {
        event.preventDefault();

        //Clear previous errors
        jQuery('.alert, .alert-danger').remove();
        //get user data and check its validity
        var FullName, Email, Message;
        FullName = jQuery('#input_FullName');
        Email = jQuery('#input_EMail');
        Message = jQuery('#input_Message');

        var ErrorMessage = '';
        //Show that data isn't valid
        if(FullName.val().length == 0)
            ErrorMessage += 'Data in your <b>Full name</b> field is not valid.<br>';
        if(Email.val().length == 0)
            ErrorMessage += 'Data in your <b>email</b> field is not valid.<br>';
        if(Message.val().length == 0)
            ErrorMessage += '<b>Message</b> cannot be empty. Please write something.'
        if(ErrorMessage.length != 0)
        {
            jQuery('.form-horizontal').before('<div class="alert alert-danger" role="alert">' + ErrorMessage + '</div>');
            return;
        }
        //perform ajax call and show status message
        jQuery.ajax
        ({
            url: '/post',
            method: 'POST',
            dataType: 'json',
            data: JSON.stringify({'FullName': FullName.val(), 'Email': Email.val(), 'Message': Message.val()}),
            success: FormSubmitSuccess,
            error: FormSubmitError,
        });

        GetSotialButtons();
    });
}

//Supmentary functions
function FormSubmitError()
{
}
function FormSubmitSuccess()
{
    jQuery('#panel_success').removeClass('hidden');
    jQuery('#panel_contact').hide(300);
}
function GetSotialButtons()
{
    stLight.options({publisher: "3cc14182-9f58-495c-a62d-f4f600e1cfbe", doNotHash: false, doNotCopy: false, hashAddressBar: false});
}
