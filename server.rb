require 'sinatra'
require 'json'

$SiteDirectory = '/home/synaps/controlit/'

post '/' do
    data = JSON.parse(request.body.read)
    open($SiteDirectory + 'requests.txt', 'a') do |file|
        file.puts("From: #{data['FullName']}")
        file.puts("Email: #{data['Email']}")
        file.puts("Message: #{data['Message']}\n" + ?-*10)
    end
    '0'
end
